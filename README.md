# README #

This repository contains code for a simple IGB App that calculates average score for the selected Syms for a track in IGB.

## Purpose of this App

This App serves as an example for developers to develop their own new Apps for IGB. Developers can use this App as a reference and make necessary changes to implement their functionalities and add a unique logo to their Apps. The bitbucket pipelines can be configured to automate the build process with the help of bitbucket_pipelines.yml and pom.xml files.

### How do I run this App? ###

There are two ways to run this App.

* * *

## Option 1: Build it on your local computer.

1.  Clone this repository
2.  Build the App by running `mvn package` on your local computer
3.  Start IGB 
4.  Select **Open App Manager** from the IGB **Tools** menu
5.  Click **Manage Repositories** button
6.  Click on **Add** to add a new App repository to install Get Average Score App
7.  Give a suitable name to your repository 
8.  Click on **Choose local folder** 
9.  Navigate to the cloned repository path and select the **target** directory inside the cloned repository as a new App repository
10. Go back to IGB App Manager. You should now see a new App named "Get Average Score" on the list of available Apps
11. Follow the instructions in the App Description visible in the IGB App Manager to run and understand the functionality of the App. 


## Option 2: Use this repository's Downloads folder as an IGB App repository

This repository's **Downloads** folder contains an OBR index file (repository.xml), which can be used to install Get Average Score App.

1.  To do this, follow the instructions above till Step 7.
2.  Instead of selecting a local folder, directly enter this URL in URL entry field:
	 https://bitbucket.org/skulka2710/get-average-score/downloads/
3.  Go back to IGB App Manager. You should now see a new App named "Get Average Score" on the list of available Apps
4.  Follow the instructions in the App Description visible in the IGB App Manager to run and understand the functionality of the App. 

### Questions or Comments? ###

Contact:

* Shamika Kulkarni - skulka26@uncc.edu or shamikagkulkarni@gmail.com
